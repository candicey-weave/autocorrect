import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LdcInsnNode
import java.io.FileOutputStream
import java.util.jar.JarEntry
import java.util.jar.JarFile
import java.util.jar.JarOutputStream

plugins {
    kotlin("jvm") version ("1.8.0")

    `maven-publish`

    id("com.github.weave-mc.weave") version ("8b70bcc707")
}

val PACKAGE = "com.gitlab.candicey.autocorrect"
val ZENITH_LOADER_VERSION = "0.1.6"
val ZENITH_CORE_VERSION = "1.4.1"
val CONCENTRA_VERSION = "0.1.2"

group = PACKAGE
version = "0.1.0"

minecraft.version("1.8.9")

val gitlabProjectId = 51056130

repositories {
    mavenCentral()
}

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://repo.polyfrost.cc/releases")
    // Zenith Core
    maven("https://gitlab.com/api/v4/projects/45235852/packages/maven")
    // Zenith Loader
    maven("https://gitlab.com/api/v4/projects/50863327/packages/maven")

    maven {
        url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
        name = "GitLab"
        credentials(HttpHeaderCredentials::class) {
            name = "Private-Token"
            value = findProperty("gitLabPrivateToken") as String?
        }
        authentication {
            create("header", HttpHeaderAuthentication::class)
        }
    }
}

dependencies {
    testImplementation(kotlin("test"))

    implementation("com.github.Weave-MC:Weave-Loader:v0.2.4")

    implementation("com.gitlab.candicey.zenithloader:Zenith-Loader:$ZENITH_LOADER_VERSION")
    implementation("com.gitlab.candicey.zenithcore:Zenith-Core:$ZENITH_CORE_VERSION")

    implementation("cc.polyfrost:oneconfig-1.8.9-forge:0.2.0-alpha+")

    implementation("org.languagetool:language-en:4.6") {
        exclude(module = "asm")
        exclude(module = "guava")
    }
}

tasks.test {
    useJUnitPlatform()
}

val relocate: TaskProvider<Task> = tasks.register("relocate") {
    val originalJar = project.tasks.getByName("jar").outputs.files.singleFile

    val slashPackage = PACKAGE.replace('.', '/')

    doLast {
        val jar = JarFile(originalJar)
        val entries = jar.entries()
        val newJar = JarOutputStream(FileOutputStream(File(originalJar.parentFile, "${originalJar.nameWithoutExtension}-relocated.jar")))

        val libsPackage = "$slashPackage/libs"
        val classNameReplaceList = mapOf<String, String>(
            "com/gitlab/candicey/zenithcore"   to "com/gitlab/candicey/zenithcore_v${ZENITH_CORE_VERSION.replace('.', '_')}",
            "com/gitlab/candicey/zenithloader" to "$libsPackage/zenithloader",
            "biz/k11i/xgboost"                 to "$libsPackage/xgboost",
            "ml/dmlc/xgboost4j"                to "$libsPackage/xgboost4j",
            "com/carrotsearch/hppc"            to "$libsPackage/hppc",
            "com/optimaize/langdetect"         to "$libsPackage/langdetect",
            "com/sun/xml/bind"                 to "$libsPackage/sun/xml/bind",
            "javax/activation"                 to "$libsPackage/javax/activation",
            "javax/measure"                    to "$libsPackage/javax/measure",
            "javax/xml/bind"                   to "$libsPackage/javax/xml/bind",
            "morfologik"                       to "$libsPackage/morfologik",
            "net/loomchild/segment"            to "$libsPackage/segment",
            "opennlp"                          to "$libsPackage/opennlp",
            "org/apache/commons/csv"           to "$libsPackage/commons/csv",
            "org/apache/commons/lang3"         to "$libsPackage/commons/lang3",
            "org/apache/commons/pool2"         to "$libsPackage/commons/pool2",
            "org/apache/commons/text"          to "$libsPackage/commons/text",
            "org/languagetool"                 to "$libsPackage/languagetool",
            "tec/uom"                          to "$libsPackage/uom",
            "tech/units/indriya"               to "$libsPackage/indriya",
        )

        val equalStringReplaceList = mapOf<String, String>(
            "@@CONCENTRA_VERSION@@" to CONCENTRA_VERSION,
            "@@ZENITH_CORE_VERSION@@" to ZENITH_CORE_VERSION,
        )
        val containStringReplaceList = mapOf<String, String>(
            "org.languagetool" to "$PACKAGE.libs.languagetool",
        )
        val skipStringReplaceClassList = listOf<String>(
        )

        val writeEntryToFile = { file: JarFile, outStream: JarOutputStream, entry: JarEntry, entryName: String ->
            outStream.putNextEntry(JarEntry(entryName))
            outStream.write(file.getInputStream(entry).readBytes())
            outStream.closeEntry()
        }

        while (entries.hasMoreElements()) {
            val entry = entries.nextElement()
            if (!entry.isDirectory) {
                when {
                    entry.name.endsWith(".class") -> {
                        val bytes = jar.getInputStream(entry).readBytes()

                        var cr = ClassReader(bytes)
                        var cw = ClassWriter(cr, 0)
                        cr.accept(ClassRemapper(cw, object : Remapper() {
                            override fun map(internalName: String): String =
                                classNameReplaceList.entries.fold(internalName) { acc, (target, replacement) -> acc.replaceFirst(target, replacement) }
                        }), 0)

                        cr = ClassReader(cw.toByteArray())
                        val cn = ClassNode()
                        cr.accept(cn, 0)

                        if (skipStringReplaceClassList.none { cn.name == it }) {
                            for (method in cn.methods) {
                                for (insn in method.instructions) {
                                    if (insn is LdcInsnNode) {
                                        if (insn.cst is String) {
                                            val cst = insn.cst as? String ?: continue
                                            if (equalStringReplaceList.containsKey(cst) || containStringReplaceList.any { (target, _) -> cst.contains(target) }) {
                                                insn.cst = containStringReplaceList.entries.fold(equalStringReplaceList[cst] ?: cst) { acc, (target, replacement) -> acc.replace(target, replacement) }
                                            }
                                        }
                                    }
                                }
                            }

                            for (field in cn.fields) {
                                if (field.value is String) {
                                    val value = field.value as? String ?: continue
                                    if (equalStringReplaceList.containsKey(value) || containStringReplaceList.any { (target, _) -> value.contains(target) }) {
                                        field.value = containStringReplaceList.entries.fold(equalStringReplaceList[value] ?: value) { acc, (target, replacement) -> acc.replace(target, replacement) }
                                    }
                                }
                            }
                        }

                        cw = ClassWriter(cr, 0)
                        cn.accept(cw)

                        newJar.putNextEntry(JarEntry(classNameReplaceList.entries.fold(entry.name) { acc, (target, replacement) -> acc.replaceFirst(target, replacement) }))
                        newJar.write(cw.toByteArray())
                        newJar.closeEntry()
                    }

                    entry.name == "weave.mod.json" -> {
                        val bytes = jar.getInputStream(entry).readBytes()
                        var string = String(bytes)

                        for ((target, replacement) in classNameReplaceList) {
                            val dotTarget = target.replace('/', '.')
                            val dotReplacement = replacement.replace('/', '.')

                            string = string.replace(dotTarget, dotReplacement)
                        }

                        newJar.putNextEntry(JarEntry(entry.name))
                        newJar.write(string.toByteArray())
                        newJar.closeEntry()
                    }

                    entry.name == "zenithloader/dependencies/${PACKAGE.substringAfterLast('.')}.versions.json" -> {
                        val bytes = jar.getInputStream(entry).readBytes()
                        var string = String(bytes)

                        for ((target, replacement) in equalStringReplaceList) {
                            string = string.replace(target, replacement)
                        }

                        newJar.putNextEntry(JarEntry(entry.name))
                        newJar.write(string.toByteArray())
                        newJar.closeEntry()
                    }

                    entry.name == "META-INF/org/languagetool/language-module.properties" || entry.name.endsWith("/grammar.xml") || entry.name.endsWith("/disambiguation.xml") -> {
                        val bytes = jar.getInputStream(entry).readBytes()
                        var string = String(bytes)

                        string = string.replace("org.languagetool", "$PACKAGE.libs.languagetool")

                        newJar.putNextEntry(JarEntry(entry.name))
                        newJar.write(string.toByteArray())
                        newJar.closeEntry()
                    }

                    entry.name.startsWith("org/languagetool") && entry.name.contains("MessagesBundle") -> writeEntryToFile(jar, newJar, entry, "$libsPackage/languagetool/${entry.name.substringAfter("org/languagetool/")}")

                    else -> writeEntryToFile(jar, newJar, entry, entry.name)
                }
            }
        }

        newJar.close()
    }

    outputs.file(originalJar.parentFile.resolve("${originalJar.nameWithoutExtension}-relocated.jar"))
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])

            val libsDirectory = File(project.projectDir, "build/libs")
            val file = libsDirectory.resolve("${project.name}-${project.version}-relocated.jar")
            artifact(file) {
                classifier = "relocated"
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

tasks.getByName("publishLibraryPublicationToMavenRepository").dependsOn(relocate)

tasks.jar {
    finalizedBy(tasks.getByName("relocate"))

    val wantedJar = listOf(
        "Zenith-Loader",
        "languagetool-core",
        "language-en",
        "segment",
        "xgboost",
        "language-detector",
        "jaxb",
        "commons-text",
        "commons-pool2",
        "commons-csv",
        "commons-lang3",
        "morfologik",
        "unit-api",
        "javax.activation-api",
        "indriya",
        "uom-lib-common",
        "opennlp",
        "hppc",
    )
    configurations["compileClasspath"]
        .filter { wantedJar.find { wantedJarName -> it.name.contains(wantedJarName) } != null }
        .forEach { file: File ->
            from(zipTree(file.absoluteFile)) {
                this.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }
        }
}

val cleanBuild: Task = tasks.create("cleanBuild") {
    dependsOn(tasks.getByName("clean"))
    finalizedBy(tasks.getByName("build"))
}
