# AutoCorrect
- A [Weave](https://github.com/Weave-MC) mod that helps you correct your spelling and grammar mistakes.

<br>

## Images
### Suggestions
![spelling](assets/spelling.png)
![grammar](assets/grammar.png)
### Dialects
![dialect](assets/dialect.png)

## Keybindings
### General
- Press `RSHIFT` to open OneConfig menu to change the dialect and toggle the mod.
### Chat Screen
- `ALT + Return` - Open the list of suggestions.
- `UP`/`TAB` - Select the next suggestion.
- `DOWN`/`SHIFT + TAB` - Select the previous suggestion.
- `Return` - Replace the word with the selected suggestion.

<br>

## Installation
1. Download the [AutoCorrect](#download) mod.
2. Place the jars in your Weave mods folder.
    1. Windows: `%userprofile%\.weave\mods`
    2. Unix: `~/.weave/mods`

<br>

## Build
- Clone the repository.
- Run `./gradlew build` in the root directory.
- The built jar file will be in `build/libs`.

<br>

## Credits
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.
- [Sk1er LLC](https://github.com/Sk1erLLC/AutoCorrect) - For the original AutoCorrect mod.

<br>

## Download
- [Package Registry](https://gitlab.com/candicey-weave/autocorrect/-/packages) - Select the latest version and download the jar file that ended with `-relocated.jar`.

<br>

## License
- Crêpes is licensed under the [GNU General Public License Version 3](LICENSE).