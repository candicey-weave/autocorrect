package com.gitlab.candicey.autocorrect.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.Dropdown
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType

object AutoCorrectConfig : Config(Mod("AutoCorrect", ModType.UTIL_QOL), "autocorrect.json") {
    /**
     * @see [com.gitlab.candicey.autocorrect.hook.AutoCorrectConfigHook]
     */
    @Dropdown(
        name = "Dialect",
        options = []
    )
    var dialect = 2

    init {
        initialize()
    }
}