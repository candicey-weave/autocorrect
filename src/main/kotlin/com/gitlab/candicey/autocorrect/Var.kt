package com.gitlab.candicey.autocorrect

import com.gitlab.candicey.autocorrect.config.AutoCorrectConfig
import com.gitlab.candicey.autocorrect.helper.DialectHelper
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.languagetool.JLanguageTool

val LOGGER: Logger = LogManager.getLogger("AutoCorrect")

val languageChecker: JLanguageTool
    get() = DialectHelper.getDialect(AutoCorrectConfig.dialect)
