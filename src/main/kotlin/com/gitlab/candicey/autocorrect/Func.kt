package com.gitlab.candicey.autocorrect

fun info(message: String) = LOGGER.info("[AutoCorrect] $message")