package com.gitlab.candicey.autocorrect.helper

import org.languagetool.MultiThreadedJLanguageTool
import org.languagetool.language.*

object DialectHelper {
    val dialectMap = mapOf(
        "American English"      to lazy { MultiThreadedJLanguageTool(AmericanEnglish()) },
        "Australian English"    to lazy { MultiThreadedJLanguageTool(AustralianEnglish()) },
        "British English"       to lazy { MultiThreadedJLanguageTool(BritishEnglish()) },
        "Canadian English"      to lazy { MultiThreadedJLanguageTool(CanadianEnglish()) },
        "New Zealand English"   to lazy { MultiThreadedJLanguageTool(NewZealandEnglish()) },
        "South African English" to lazy { MultiThreadedJLanguageTool(SouthAfricanEnglish()) },
    )

    private val dialectInstances = dialectMap.values.toList()

    fun getDialect(index: Int) = dialectInstances[index].value
}