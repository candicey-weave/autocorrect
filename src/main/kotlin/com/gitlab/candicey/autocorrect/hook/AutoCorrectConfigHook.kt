package com.gitlab.candicey.autocorrect.hook

import cc.polyfrost.oneconfig.config.annotations.Dropdown
import com.gitlab.candicey.autocorrect.helper.DialectHelper
import com.gitlab.candicey.zenithcore.extension.toDescriptor
import com.gitlab.candicey.zenithcore.util.weave.named
import net.weavemc.loader.api.Hook
import org.objectweb.asm.tree.ClassNode

/**
 * @see [com.gitlab.candicey.autocorrect.config.AutoCorrectConfig]
 */
object AutoCorrectConfigHook : Hook("com/gitlab/candicey/autocorrect/config/AutoCorrectConfig") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val dropDownAnnotationDescriptor = Dropdown::class.java.toDescriptor()
        val dropdownAnnotation = node
            .fields
            .named("dialect")
            .visibleAnnotations
            .find { it.desc == dropDownAnnotationDescriptor }
            ?: error("Dropdown annotation not found")

        val dialectArray = DialectHelper.dialectMap.keys.toList()

        for ((index, value) in dropdownAnnotation.values.withIndex()) {
            if (value == "options") {
                dropdownAnnotation.values[index + 1] = dialectArray
            }
        }
    }
}