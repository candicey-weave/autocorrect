package com.gitlab.candicey.autocorrect.hook

import com.gitlab.candicey.autocorrect.gui.AutoCorrectEnabledInputGui
import com.gitlab.candicey.zenithcore.util.weave.dump
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.*

/**
 * @see [net.minecraft.client.gui.GuiChat]
 */
object GuiChatHook : Hook("net/minecraft/client/gui/GuiChat") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        transformInitGui(node)
        transformKeyTyped(node)
    }

    private fun transformInitGui(classNode: ClassNode) {
        val methodNode = classNode.methods.named("initGui")
        val instructions = methodNode.instructions

        val newGuiTextFieldInstance = instructions
            .filterIsInstance<TypeInsnNode>()
            .find { it.opcode == Opcodes.NEW && it.desc == "net/minecraft/client/gui/GuiTextField" }
            ?: error("Cannot find NEW GuiTextField instruction in GuiChat#initGui")
        newGuiTextFieldInstance.desc = internalNameOf<AutoCorrectEnabledInputGui>()

        val guiTextFieldConstructor = instructions
            .filterIsInstance<MethodInsnNode>()
            .find { it.opcode == Opcodes.INVOKESPECIAL && it.owner == "net/minecraft/client/gui/GuiTextField" && it.name == "<init>" }
            ?: error("Cannot find INVOKESPECIAL GuiTextField constructor in GuiChat#initGui")
        guiTextFieldConstructor.owner = internalNameOf<AutoCorrectEnabledInputGui>()
    }

    private fun transformKeyTyped(classNode: ClassNode) {
        val methodNode = classNode.methods.named("keyTyped")
        val instructions = methodNode.instructions

        val labelNode = LabelNode()

        val insertBeforeOffset = { insn: AbstractInsnNode, offset: Int ->
            var targetInsn: AbstractInsnNode = insn

            repeat(offset) { targetInsn = targetInsn.previous }

            instructions.insert(targetInsn, asm {
                aload(0)
                getfield("net/minecraft/client/gui/GuiChat", "inputField", "Lnet/minecraft/client/gui/GuiTextField;")
                iload(1)
                iload(2)
                invokevirtual("net/minecraft/client/gui/GuiTextField", "textboxKeyTyped", "(CI)Z")
                ifne(labelNode)
            })
        }

        val invokevirtuals = instructions
            .filterIsInstance<MethodInsnNode>()
            .filter { it.opcode == Opcodes.INVOKEVIRTUAL }

        val insert1 = mutableListOf<MethodInsnNode>()
        val insert2 = mutableListOf<MethodInsnNode>()
        for (insn in invokevirtuals) {
            when (insn.name) {
                "getSentHistory", "sendChatMessage" -> insert1.add(insn)
                "autocompletePlayerNames" -> insert2.add(insn)
            }
        }
        for (methodInsnNode in insert1) {
            insertBeforeOffset(methodInsnNode, 3)
        }
        for (methodInsnNode in insert2) {
            insertBeforeOffset(methodInsnNode, 2)
        }

        val returns = instructions.filter { it.opcode == Opcodes.RETURN }
        for (insn in returns) {
            instructions.insertBefore(insn, labelNode)
        }
    }
}