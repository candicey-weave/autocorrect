package com.gitlab.candicey.autocorrect

import com.gitlab.candicey.autocorrect.config.AutoCorrectConfig
import com.gitlab.candicey.autocorrect.hook.AutoCorrectConfigHook
import com.gitlab.candicey.autocorrect.hook.GuiChatHook
import com.gitlab.candicey.zenithcore.extension.add
import com.gitlab.candicey.zenithcore.helper.HookManagerHelper
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.Dependencies.concentra
import net.weavemc.loader.api.ModInitializer
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.event.StartGameEvent

class AutoCorrectMain : ModInitializer {
    override fun preInit() {
        info("Initialising...")

        ZenithLoader.loadDependencies(
            // autocorrect.versions.json
            concentra("autocorrect"),
        )

        HookManagerHelper.hooks.add(
            AutoCorrectConfigHook,
            GuiChatHook,
        )

        EventBus.subscribe(StartGameEvent.Post::class.java) { AutoCorrectConfig }

        info("Initialised!")
    }
}