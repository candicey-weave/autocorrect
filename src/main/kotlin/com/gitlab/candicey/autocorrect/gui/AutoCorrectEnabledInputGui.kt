package com.gitlab.candicey.autocorrect.gui

import com.gitlab.candicey.autocorrect.config.AutoCorrectConfig
import com.gitlab.candicey.autocorrect.extension.height
import com.gitlab.candicey.autocorrect.extension.lineScrollOffset
import com.gitlab.candicey.autocorrect.languageChecker
import com.gitlab.candicey.zenithcore.fontRenderer
import com.google.common.collect.Lists
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.gui.GuiTextField
import org.languagetool.rules.RuleMatch
import org.languagetool.rules.UppercaseSentenceStartRule
import org.languagetool.rules.patterns.PatternRule
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Mouse
import java.util.concurrent.atomic.AtomicBoolean
import java.util.function.Consumer
import java.util.stream.Collectors
import kotlin.math.max
import kotlin.math.min

class AutoCorrectEnabledInputGui(
    componentId: Int,
    fontrendererObj: FontRenderer?,
    x: Int,
    y: Int,
    par5Width: Int,
    par6Height: Int
) : GuiTextField(componentId, fontrendererObj, x, y, par5Width, par6Height) {
    private val updating = AtomicBoolean()
    private var currentIssues: MutableList<RuleMatch>? = ArrayList()
    private var lastTypeTime = System.currentTimeMillis()
    private var suggestions: List<String>? = null
    private var currentMatch: RuleMatch? = null
    private var selectedSuggestion = -1
    private val NO_SUGGESTIONS = Lists.asList("No suggestions!", arrayOf())
    private var lastMouseX = 0
    private var lastMouseY = 0
    private var iterating = false
    private var iterIndex = -1
    private val fontRendererInstance = fontRenderer

    val enabled: Boolean
        get() = AutoCorrectConfig.enabled
    override fun drawTextBox() {
        super.drawTextBox()

        if (!enabled) {
            return
        }

        if (visible && System.currentTimeMillis() - lastTypeTime > 250) {
            updateSpellcheck()
            lastTypeTime += 20000
        }
        val s = this.fontRendererInstance.trimStringToWidth(text.substring(lineScrollOffset), width)
        if (suggestions != null) {
            var longest = ""
            val realSuggestions = suggestions!!.ifEmpty { NO_SUGGESTIONS }
            for (sugg in realSuggestions) {
                if (sugg.length > longest.length) longest = sugg
            }
            val boxWidth = 3 + fontRendererInstance.getStringWidth(longest)
            val boxHeight = 3 + realSuggestions.size * (fontRendererInstance.FONT_HEIGHT + 2)
            val bottomY = yPosition + (height - 8) / 2 - 5
            val charOffset = cursorPosition - lineScrollOffset
            val x = xPosition + fontRendererInstance.getStringWidth(s.substring(0, charOffset))
            drawRect(x, bottomY - boxHeight, x + boxWidth, bottomY, -0x1000000)
            var startY = bottomY - 2 - fontRendererInstance.FONT_HEIGHT
            val mX = Mouse.getEventX() * width / Minecraft.getMinecraft().displayWidth
            val mY =
                Minecraft.getMinecraft().currentScreen.height - Mouse.getEventY() * Minecraft.getMinecraft().currentScreen.height / Minecraft.getMinecraft().displayHeight - 1
            if (mX >= x && mX <= x + boxWidth && mY >= bottomY - boxHeight && mY <= bottomY && (mX != lastMouseX || mY != lastMouseY)) {
                val insideY = mY - (bottomY - boxHeight)
                val clickIndex = insideY / (fontRendererInstance.FONT_HEIGHT + 2)
                selectedSuggestion = suggestions!!.size - 1 - clickIndex
                lastMouseX = mX
                lastMouseY = mY
            }
            for ((index, sugg) in realSuggestions.withIndex()) {
                val color = if (index == selectedSuggestion) -0x813e7 else -0x121213
                fontRendererInstance.drawString(sugg, x + 2, startY, color)
                startY -= fontRendererInstance.FONT_HEIGHT + 2
            }
        }
        if (currentIssues != null) {
            val allowedFrom = lineScrollOffset
            val allowedTo = lineScrollOffset + s.length
            val beginX = if (enableBackgroundDrawing) xPosition + 4 else xPosition
            var y = if (enableBackgroundDrawing) yPosition + (height - 8) / 2 else yPosition
            y += fontRendererInstance.FONT_HEIGHT + 1
            for (issue in currentIssues!!) {
                if (issue.fromPos < allowedFrom || issue.toPos > allowedTo) continue
                val offX = fontRendererInstance.getStringWidth(text.substring(0, issue.fromPos))
                val xLength = fontRendererInstance.getStringWidth(text.substring(issue.fromPos, issue.toPos))
                drawRect(beginX + offX, y, beginX + offX + xLength, y + 1, -0xfdbf6)
            }
        }
    }

    override fun setText(text: String) {
        super.setText(text)
        currentIssues = null
        suggestions = null
        currentMatch = null
        selectedSuggestion = -1
        iterating = false
        iterIndex = -1

        if (!enabled) {
            return
        }

        updateSpellcheck()
    }

    override fun writeText(text: String) {
        super.writeText(text)
        if (cursorPosition >= text.length) return
        currentIssues = null
        suggestions = null
        currentMatch = null
        selectedSuggestion = -1

        if (!enabled) {
            return
        }

        updateSpellcheck()
    }

    override fun textboxKeyTyped(char: Char, keyCode: Int): Boolean {
        if (enabled) {
            if (keyCode == Keyboard.KEY_ESCAPE && (iterating || suggestions != null)) {
                iterating = false
                iterIndex = -1
                updateSpellcheck()
                currentMatch = null
                suggestions = null
                selectedSuggestion = -1
                return true
            }
            if (keyCode == Keyboard.KEY_RETURN && (Keyboard.isKeyDown(Keyboard.KEY_LMENU) || Keyboard.isKeyDown(Keyboard.KEY_RMENU)) && currentIssues != null && currentIssues!!.size > 0) {
                if (iterating) {
                    iterIndex++
                    if (iterIndex >= currentIssues!!.size) {
                        iterating = false
                        iterIndex = -1
                        updateSpellcheck()
                        setCursorPosition(text.length)
                        return true
                    }
                    cursorPosition = currentIssues!![iterIndex].fromPos
                    setSelectionPos(cursorPosition)
                    generateSuggestions(cursorPosition)
                    return true
                }
                iterating = true
                iterIndex = 0
                cursorPosition = currentIssues!![iterIndex].fromPos
                setSelectionPos(cursorPosition)
                generateSuggestions(cursorPosition)
                return true
            }
            if (keyCode == Keyboard.KEY_RETURN && suggestions != null) {
                if (suggestions!!.size - 1 < selectedSuggestion || selectedSuggestion < 0) return true
                completeSuggestion()
                return true
            }
        }
        val typed = super.textboxKeyTyped(char, keyCode)
        if (enabled) {
            if (keyCode == Keyboard.KEY_SPACE) {
                updateSpellcheck()
            } else if ((keyCode == Keyboard.KEY_UP || (keyCode == Keyboard.KEY_TAB && !(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)))) && suggestions != null) {
                selectedSuggestion = min((selectedSuggestion + 1).toDouble(), (suggestions!!.size - 1).toDouble())
                    .toInt()
                return true
            } else if ((keyCode == Keyboard.KEY_DOWN || keyCode == Keyboard.KEY_TAB) && suggestions != null) {
                selectedSuggestion = max((selectedSuggestion - 1).toDouble(), 0.0).toInt()
                return true
            }
        }
        if (typed) {
            lastTypeTime = System.currentTimeMillis()
        }
        return typed
    }

    override fun mouseClicked(mouseX: Int, mouseY: Int, button: Int) {
        super.mouseClicked(mouseX, mouseY, button)

        if (!enabled) {
            return
        }

        val flag = mouseX >= xPosition && mouseX < xPosition + width && mouseY >= yPosition && mouseY < yPosition + height
        if (suggestions != null) {
            var longest = ""
            for (sugg in suggestions!!) {
                if (sugg.length > longest.length) longest = sugg
            }
            val boxWidth = 2 * 2 + fontRendererInstance.getStringWidth(longest)
            val boxHeight = 2 * 2 + suggestions!!.size * (fontRendererInstance.FONT_HEIGHT + 2)
            val bottomY = yPosition + (height - 8) / 2 - 4
            val s = this.fontRendererInstance.trimStringToWidth(text.substring(lineScrollOffset), width)
            val charOffset = cursorPosition - lineScrollOffset
            val x = xPosition + fontRendererInstance.getStringWidth(s.substring(0, charOffset))
            if (mouseX < x || mouseX > x + boxWidth || mouseY < bottomY - boxHeight || mouseY > bottomY) {
                suggestions = null
            } else if (selectedSuggestion < suggestions!!.size && selectedSuggestion >= 0) {
                completeSuggestion()
            }
        }

        // We're in the text box, we're focused, and we right clicked.
        if (isFocused && flag && button == 1) {
            var i = mouseX - xPosition
            if (enableBackgroundDrawing) {
                i -= 4
            }
            val s = this.fontRendererInstance.trimStringToWidth(text.substring(lineScrollOffset), width)
            val clickPosition = this.fontRendererInstance.trimStringToWidth(s, i).length + lineScrollOffset
            setCursorPosition(clickPosition)
            updateSpellcheck()
            generateSuggestions(clickPosition)
        }
    }

    private fun completeSuggestion() {
        try {
            val oldText = text.substring(currentMatch!!.fromPos, currentMatch!!.toPos)
            val newText = suggestions!![selectedSuggestion]
            text = (text.substring(0, currentMatch!!.fromPos)
                    + newText
                    + text.substring(currentMatch!!.toPos))
            val lengthDiff = newText.length - oldText.length
            if (currentIssues == null) {
                return
            }
            currentIssues!!.removeIf { it: RuleMatch -> it == currentMatch }
            currentIssues!!.forEach(Consumer { it: RuleMatch ->
                if (it.fromPos > currentMatch!!.toPos) it.setOffsetPosition(
                    it.fromPos + lengthDiff,
                    it.toPos + lengthDiff
                )
            })
            val tmp = currentMatch
            currentMatch = null
            suggestions = null
            selectedSuggestion = -1
            if (!iterating) {
                setCursorPosition(tmp!!.fromPos + newText.length)
            } else {
                if (iterIndex >= currentIssues!!.size) {
                    iterating = false
                    iterIndex = -1
                    setCursorPosition(text.length)
                    updateSpellcheck()
                    return
                }
                setCursorPosition(currentIssues!![iterIndex].fromPos)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun setCursorPosition(cursorPosition: Int) {
        super.setCursorPosition(cursorPosition)

        if (!enabled) {
            return
        }

        if (currentMatch != null) {
            if (cursorPosition < currentMatch!!.fromPos || cursorPosition > currentMatch!!.toPos) {
                currentMatch = null
                suggestions = null
                selectedSuggestion = -1
            }
        } else if (currentIssues != null) {
            generateSuggestions(cursorPosition)
        }
    }

    private fun generateSuggestions(cursorPosition: Int) {
        for (match in currentIssues!!) {
            if (match.fromPos <= cursorPosition && cursorPosition <= match.toPos) {
                // Show options popup
                suggestions = match.getSuggestedReplacements().stream().limit(20).collect(Collectors.toList())
                currentMatch = match
                selectedSuggestion = 0
                break
            }
        }
    }

    private fun updateSpellcheck() {
        if (updating.compareAndSet(false, true)) {
            Thread {
                try {
                    currentIssues = languageChecker.check(text)
                        .stream()
                        .filter {
                            if (it.rule is UppercaseSentenceStartRule) return@filter false
                            if (it.rule is PatternRule) {
                                return@filter !(it.rule as PatternRule).fullId.startsWith("I_LOWERCASE")
                            }
                            true
                        }
                        .collect(Collectors.toList())
                    val validSuggestions = currentMatch != null && currentIssues!!
                        .stream()
                        .anyMatch { it: RuleMatch -> cursorPosition >= it.fromPos && cursorPosition <= it.toPos && it.getSuggestedReplacements() == currentMatch!!.getSuggestedReplacements() }
                    if (!validSuggestions) {
                        currentMatch = null
                        suggestions = null
                        selectedSuggestion = -1
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    updating.set(false)
                }
            }.start()
        }
    }
}
