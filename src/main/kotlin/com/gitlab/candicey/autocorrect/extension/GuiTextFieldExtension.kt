package com.gitlab.candicey.autocorrect.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.gui.GuiTextField

val GuiTextField.height: Int by ShadowField()

val GuiTextField.lineScrollOffset: Int by ShadowField()